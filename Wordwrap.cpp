// Jack Moore 30 Nov 2013
// Wordwrap.cpp
// Function definitions for Wordwrap library



#include "Wordwrap.h"



Wordwrap::Wordwrap() : m_out(std::cout)
{
  m_limit = DEFAULT_LIMIT;
  m_column = 0;
  m_tab = DEFAULT_TAB;
  m_autoTab = true;
  return;
}


Wordwrap::Wordwrap(std::ostream& out, const unsigned int limit, 
                   const unsigned int tab, const bool autoTab) : m_out(out)
{
  m_limit = limit;
  m_column = 0;
  m_tab = tab;
  m_autoTab = autoTab;
  return;
}
