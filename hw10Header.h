// Jack Moore 28 Nov 2013
// hw10Header.h
// Header for main driver of hw10 and its function prototypes



#ifndef HW10HEADER_H
#define HW10HEADER_H



#include <cstdlib>
#include <string>
#include "Business.h"
#include "Customer.h"



const int CUST_NUM = 20;
const int MAX_LEVEL = 20;
const int BUS1_INC = -1;
const int BUS2_INC = 1;
const int MIN_HAP = 10;
const int MAX_HAP = 90;
const int MAX_SHUFFLE = 20;
const char DELIM = ',';
const char MOES[] = "Moe\'s Bar";
const char COMICS[] = "Comic Book Guy\'s Store";
const char MOE_STOCK[] = "barInventory.txt";
const char COMIC_STOCK[] = "comicInventory.txt";
const char CUST_DATA[] = "people.txt";



// Desc Reads in customer data from a text file into the array of customers
// Pre  custNum must be the length of custArray, format of data goes as
//      NAME, INCLINATION [NEWLINE] // INCLINATION being a textual
//      representation of an integer
// Post CustArray has its contents modified with new data

void assignCustData(Customer custArray[], const int custNum, const char data[]);


// Desc Adds customers to businesses depending on their inclination
// Pre  custNum must be the length of custArray, For inclination values of
//      BUS1_INC will go into bus1 and BUS2_INC will go to bus2
// Post Businesses are filled with customers

void chooseBusiness(const Customer custArray[], const int custNum, 
                    Business& bus1, Business& bus2);


// Desc Has all customers leave the business and collects them into a single
//      array
// Pre  custNum must be the length of custArray
// Post custArray filled with customers

void collectCustomers(Customer custArray[], const int custNum, Business& bus1,
                      Business& bus2);


// Desc Parses through the array of customers and has them each choose another
//      random customer. They will either throw or steal something depending on
//      their inclination
// Pre  custNum must be the length of custArray
// Post CustArray has its contents modified, text will be outputted to the
//      screen

void interact(Customer custArray[], const int custNum);


// Desc Parses through each customer and if their happiness is either too high
//      or too low they are shipped off to Shelbyville. custArray is filled with
//      the remaining people
// Pre  custNum must be the length of custArray
// Post custArray will be modified, custNum is its new length, text will be
//      outputted to the screen

void evaluateFate(Customer custArray[], int& custNum, const int level);


// Desc Determines whether the passed noun should be prefixed with either a or
//      an
// Pre  The first char of noun should be alphanum
// Post string returned that is identical to noun with the exception that it is
//      prefixed with either a or an

std::string aOrAn(const std::string& noun);


// Desc Takes an array and shuffles its contents
// Pre  rand should be seeded, size should be the size of the array, number of
//      elements cannot be greater than MAX_SHUFFLE
// Post The array is shuffled

template <typename T>
void shuffleArray(T array[], const int size)
{
  int position;

  // Create new array to temporarily place shuffle results
  T dest[MAX_SHUFFLE];

  // Create array of bools to allow for rand no repeat
  bool noRepeat[size];

  // Set all bools to false
  for (int i = 0; i < size; i++)
    noRepeat[i] = false;

  // Shuffle the contents
  for (int i = 0; i < size; i++)
  {
    // Pick random position no repeat
    do
    {
      position = rand() % size;
    } while (noRepeat[position]);

    // Mark position as picked
    noRepeat[position] = true;

    // Place data in new random position
    dest[i] = array[position];
  }

  // Copy data back into original array
  for (int i = 0; i < size; i++)
    array[i] = dest[i];

  return;
}



#endif
