// Jack Moore 26 Nov 2013
// Product.h
// Header for product struct 



#ifndef PRODUCT_H
#define PRODUCT_H



#include <string>



struct product
{
  std::string m_name;
  float m_price;
};



#endif
