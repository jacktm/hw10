// Jack Moore 28 Nov 2013
// main.cpp
// Main driver for hw10



#include <iostream>
#include "Business.h"
#include "Customer.h"
#include "hw10Header.h"



int main()
{
  int numCust = CUST_NUM, level = 0;

  // Create two businesses
  Business bar = Business(MOES, 0.0, MOE_STOCK);
  Business comic = Business(COMICS, 0.0, COMIC_STOCK);

  // Create 20 Customers
  Customer custArray[CUST_NUM];

  //Output version number
  std::cout << "\n\nSpringfield Simulation (Version 1.2.1) by Jack Moore" 
            << std::endl;

  //seed random
  srand(time(NULL));

  //double digit precision
  std::cout.setf(std::ios::fixed);
  std::cout.setf(std::ios::showpoint); 
  std::cout.precision(2);

  // Assign data to customers
  assignCustData(custArray, CUST_NUM, CUST_DATA);

  ///////////////////////
  // OUTPUT 1
  ///////////////////////
  std::cout << "\n\nInitial Customer List:\n";
  for (int i = 0; i < CUST_NUM; i++)
    std::cout << '\t' << custArray[i] << std::endl;
  std::cout << std::endl;

  do
  {
    // Update level
    level++;

    // Customers enter business depending on inclination
    chooseBusiness(custArray, numCust, bar, comic);

    std::cout << "\nLEVEL " << level << "\n\nPurchases:\n";

    // Sell products
    bar.sell_stuff();
    comic.sell_stuff();

    std::cout << "\nInteractions:\n";

    // Have customers leave all businesses
    collectCustomers(custArray, numCust, bar, comic);

    // Shuffle the array
    shuffleArray(custArray, numCust);

    // Have customers interact on the street
    interact(custArray, numCust);

    std::cout << "\nDepartures:\n";

    // Evaluate fate
    evaluateFate(custArray, numCust, level);

    std::cout << "\nCurrent Customer List:\n";

    ///////////////////////
    // OUTPUT 5
    ///////////////////////
    for (int i = 0; i < numCust; i++)
      std::cout << '\t' << custArray[i] << std::endl;

    std::cout << std::endl;

  } while (level < MAX_LEVEL && numCust > 1);

  std::cout << "\nFINAL RESULTS after level " << level 
            << "\n\nRemaining People:\n";

  ///////////////////////
  // OUTPUT 6
  ///////////////////////
  for (int i = 0; i < numCust; i++)
    std::cout << '\t' << custArray[i] << std::endl;

  std::cout << "\nBusinesses:\n\n";

  bar.print();
  std::cout << std::endl;
  comic.print();

  std::cout << std::endl;

  return 0;

}
