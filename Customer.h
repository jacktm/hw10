// Jack Moore 11 Nov 2013
// Customer.h
// Header for Customer class



#ifndef CUSTOMER_H
#define CUSTOMER_H



#include <string>
#include <cstring>
#include "Product.h"



const char DEFAULT_NAME[] = "Customer";
const float MIN_MON = 4.0;
const float MAX_MON = 250.0;
const short MAX_PURCH = 20;
const short MAX_NAME = 30;
const short DESPAIR = 0;
const short NIRVANA = 100;
const short BUY_HAPP_INC = 15;
const short BUY_HAPP_DEC = 10;
const short THROW_HAPP_INC = 5;
const short TARGET_HAPP_DEC = 20;
const short THROW_FAIL_HAPP_DEC = 25;
const short STEAL_HAPP_INC = 25;
const short VICTIM_HAPP_DEC = 20;
const short STEAL_FAIL_HAPP_DEC = 5;



class Customer
{
  private:
    product m_purchases[MAX_PURCH];
    short m_purchNum;
    float m_money;
    char m_name[MAX_NAME];
    short m_happiness;
    int m_inclination;

  public:


    // Desc Default constructor, sets an empty list of purchases, default name,
    //      and a random amount of money between $4.00 and $250
    // Pre  rand should already be initialized 
    // Post Purchases is empty, default name, random money

    Customer();


    // Desc Makes a purchases and if number of purchases is less than MAX_PURCH 
    // and a 50/50 check returns true it is successful. Also the purchase list
    // is updated. Additionally a customer's happiness is updated whether or not
    // they are able to purchase an item
    // Pre  None
    // Post If purchase is successful true is returned, purchase list updated

    bool buy_something(const product& item);


    // Desc Gets the name of the customer by copying in the name of the customer
    //      into a null terminated array
    // Pre  dest should be at least 30 chars in size
    // Post name is copied into dest

    void getName(char dest[]) const {strcpy(dest, m_name); return;}


    // Desc Sets the name of the Customer
    // Pre  name cannot be longer than 30 chars, name must be null terminated
    // Post Name of Customer modified

    void setName(const char name[]) {strcpy(m_name, name); return;}


    // Desc Returns the money of the customer
    // Pre  None
    // Post Money is returned

    float getMoney() const {return m_money;}


    // Desc Sets the money of the customer
    // Pre  None
    // Post Money of the customer is modified

    void setMoney(const float newMoney) {m_money = newMoney; return;}


    // Desc Returns the inclination of the customer
    // Pre  None
    // Post Inclination is returned

    int getInclination() const {return m_inclination;}


    // Desc sets the inclination of the Customer
    // Pre  None
    // Post Inclination of customer is modified

    void setInclination(const int incl) {m_inclination = incl; return;}

    // Desc Returns the happiness of the Customer
    // Pre  None
    // Post happiness is returned

    int getHappiness() const {return m_happiness;}


    // Desc Customer will throw item at another customer and if they have
    //      something to throw his happiness is increased by 5 and the target's
    //      is decreased by 20. If throw fails happiness goes down by 25
    // Pre  None
    // Post Number of items of items of this is affected. Happiness of both this
    //      and target is affected

    void throwItem(Customer& target);


    // Desc Customer will try to steal an item from a customer and if the victim
    //      has an item to steal and this has room for the theft is completed.
    //      If successful this' happiness increases by 25 and the victim's is
    //      decreased by 20. If the throw is unsuccessful this' happiness
    //      decreases by 5
    // Pre  None
    // Post Number of items and happiness of both Customers is affected

    void stealItem(Customer& victim);


    // Desc Outputs the Customer to a stream
    // Pre  stream must be writable
    // Post Stream written to, stream returned

    friend std::ostream & operator << (std::ostream & out, 
                                       const Customer & source);

};



#endif
