// Jack Moore 30 Nov 2013
// Wordwrap.h
// Header for wordwrap library



#ifndef WORDWRAP_H
#define WORDWRAP_H



#include <sstream>
#include <iostream>



const unsigned int DEFAULT_LIMIT = 80;
const unsigned int DEFAULT_TAB = 8;



class Wordwrap
{
private:
  unsigned int m_limit;
  std::ostream& m_out;
  unsigned int m_column;
  unsigned int m_tab;
  bool m_autoTab;

public:

  // Desc Default constructor that sets the ostream as cout, the limit as
  //      DEFAULT_LIMIT, m_tab as DEFAULT_TAB, and m_autoTab to true
  // Pre  None
  // Post ostream set to cout, limit set to DEFAULT_LIMIT, tab set to 
  //      DEFAULT_TAB, and autoTab set to true

  Wordwrap();


  // Desc Constructor that takes arguments for an ostream, a limit, a tab size,
  //      and a bool for autoTab
  // Pre  None
  // Post ostream, limit, tab, and autoTab set

  Wordwrap(std::ostream& out, const unsigned int limit, const unsigned int tab, 
           const bool autoTab);


  // Desc Outputs data to the specified output stream using proper word wrapping
  // Pre  Type T must have the << operator defined
  // Post Text written to the output stream and the column is advanced

  template <typename T>
  friend Wordwrap& operator << (Wordwrap& wrap, const T& text)
  {
    std::stringstream format;
    std::string clean;
    int length = 0;
    int max = 0;
    int wordLength;

    // Write textual data to stringstream
    format << text;

    // Write back to string
    clean = format.str();

    // Parse through every character one at a time
    for (int i = 0; i < clean.length(); i++)
    {
      // First check for spaces
      if (clean[i] == ' ')
      {
        if (wrap.m_column == wrap.m_limit)
        {
          if (wrap.m_autoTab)
          {
            wrap.m_out << "\n\t";
            wrap.m_column = wrap.m_tab;
          }
          else
          {
            wrap.m_out << "\n";
            wrap.m_column = 0;
          }
        }
        else
        {
          wrap.m_out << ' ';
          wrap.m_column++;
        }
      }

      // Else check for tabs
      else if (clean[i] == '\t')
      {
        // If the tab fits in the current line
        if (wrap.m_column + wrap.m_tab <= wrap.m_limit)
        {
          wrap.m_out << '\t';
          wrap.m_column += wrap.m_tab;
        }
        // Else create a newline
        else
        {
          wrap.m_out << "\n\t";
          wrap.m_column = wrap.m_tab;
        }
      }

      // Check if character is a new line
      // No support for bullshit windows line endings
      else if (clean[i] == '\n')
      {
        // Pretty straightforward here
        wrap.m_out << '\n';
        wrap.m_column = 0;
      }

      // Check for characters that aren't printable
      else if (clean[i] == '\b' || clean[i] == '\a' || clean[i] == '\0')
      {
        // Once again pretty simple
        wrap.m_out << clean[i];
        // Column not advanced
      }

      // Finally one space printable characters!!
      // The meat and potatoes
      else
      {
        // Set word length and counter as 0
        wordLength = 0;

        do
        {
          wordLength++;
        } while (!(clean[i+wordLength] == '\t' || clean[i+wordLength] == '\n' 
                   || clean[i+wordLength] == '\b' || clean[i+wordLength] == 
                   '\a' || clean[i+wordLength] == '\0' || clean[i+wordLength] 
                   == ' '));

        // If word doesn't overflow, write char normally
        if (wrap.m_column + wordLength <= wrap.m_limit)
        {
          wrap.m_out << clean[i];
          wrap.m_column++;
        }
        // Otherwise newline
        else
        {
          if (wrap.m_autoTab)
          {
            wrap.m_out << "\n\t" << clean[i];
            wrap.m_column = wrap.m_tab + 1;
          }
          else
          {
            wrap.m_out << '\n' << clean[i];
            wrap.m_column = 1;
          }
        }
      }
    }

    return wrap;
  }

};


#endif
