// Jack Moore 11 Nov 2013
// Customer.h
// Header for Business class



#ifndef BUSINESS_H
#define BUSINESS_H



#include <string>
#include "Customer.h"
#include "Product.h"



const short ITEMS = 10;
const short CUSTOMERS = 10;
const float SALE_PRICE = 40.0;



class Business
{
  private:
    std::string m_name;
    float m_money;
    product m_inventory[ITEMS];
    Customer m_shoppers[CUSTOMERS];
    short m_invNum;
    short m_shopNum;

  public:


    // Desc Default constructor that sets business name to an empty string, sets
    //      the money to zero, and the sizes of all arrays to zero
    // Pre  None
    // Post All variables initialized

    Business();


    // Desc Constructor that takes arguments for the name of the business, money
    //      in the cash register, and the name of a text file containing a list
    //      of inventory items
    // Pre  uName must be a null terminated, inv must be a null terminated name
    //      of an existing ascii text file in the working directory. The file
    //      that inv names cannot have more than 10 unix style lines
    // Post All variables initialized 

    Business(const char uNmae[], const float uMoney, const char inv[]);


    // Desc Prints the data of the business and the customers within
    // Pre  None
    // Post Text is outputted to the screen

    void print() const;


    // Desc Adds a customer to the Business object
    // Pre  Only 10 Customer objects may be added to the Business
    // Post A Customer object is copied in

    void addCustomer(const Customer& newCust);


    // Desc Calls the purchase function of every customer in the business and
    //      updates cash if necessary
    // Pre  rand should be seeded
    // Post Cash may be added the to the register and the internal Customers may
    //      have their data updates with new items and less money

    void sell_stuff();


    // Desc Makes all customers leave the business and puts them into an array
    // Pre  Number of customers cannot be greater than the length of custArray
    // Post custArray filled with Customers, custNum holds the number of
    //      customers in the business, there are no longer any customers in the
    //      business

    void customers_leave(Customer custArray[], int& custNum);

};



#endif
