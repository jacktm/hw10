// Jack Moore 29 Nov 2013
// hw10FuncDefs.cpp
// Function definitions for the main driver of hw10



#include <cctype>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include "Business.h"
#include "Customer.h"
#include "hw10Header.h"
#include "Wordwrap.h"



void assignCustData(Customer custArray[], const int custNum, const char data[])
{
  std::ifstream fin;
  int counter = 0, tempInc;
  char tempString[256];

  fin.open(data);

  if (!fin)
  {
    std::cout << "Fatal Error: Could not open file " << data << std::endl;
    exit(1);
  }

  while (!fin.eof() && counter < custNum)
  {
    // Get name of the Customer
    fin.getline(tempString, 256, DELIM);

    // Get inclination
    fin >> tempInc;

    // Flush line
    fin.ignore(256, '\n');

    // Assign data to Customer
    custArray[counter].setName(tempString);
    custArray[counter].setInclination(tempInc);

    counter++;
  }

  fin.close();

  return;
}


void chooseBusiness(const Customer custArray[], const int custNum, 
                    Business& bus1, Business& bus2)
{
  for (int i = 0; i < custNum; i++)
  {
    if (custArray[i].getInclination() == BUS1_INC)
      bus1.addCustomer(custArray[i]);
    else if (custArray[i].getInclination() == BUS2_INC)
      bus2.addCustomer(custArray[i]);
  }

  return;
}


void collectCustomers(Customer custArray[], const int custNum, Business& bus1,
                      Business& bus2)
{
  // Create two temporary arrays to hold Customers
  Customer temp1[CUST_NUM];
  Customer temp2[CUST_NUM];

  int temp1Size, temp2Size;

  // Collect Customers into separate arrays
  bus1.customers_leave(temp1, temp1Size);
  bus2.customers_leave(temp2, temp2Size);

  // Copy first array into custArray
  for (int i = 0; i < temp1Size; i++)
    custArray[i] = temp1[i];

  // Copy second array into custArray
  for (int i = 0; i < temp2Size; i++)
    custArray[temp1Size + i] = temp2[i];

  return;
}


void interact(Customer custArray[], const int custNum)
{
  int choice;

  for (int i = 0; i < custNum; i++)
  {
    // Pick random Customer
    choice = rand() % custNum;

    // Check inclination and throw or steal based off that
    if (custArray[i].getInclination() == custArray[choice].getInclination())
      custArray[i].stealItem(custArray[choice]);
    else
      custArray[i].throwItem(custArray[choice]);
  }

  return;
}


void evaluateFate(Customer custArray[], int& custNum, const int level)
{
  int survivors = 0, copyNum = custNum;
  char name[30];
  Wordwrap wrap;

  // Reset custNum
  custNum = 0;

  for (int i = 0; i < copyNum; i++)
  {
    // Get name of customer
    custArray[i].getName(name);

    ///////////////////////
    // OUTPUT 4
    ///////////////////////
    if (custArray[i].getHappiness() < MIN_HAP)
      wrap << '\t' << name << " falls desperately hopeless at level " 
                << level << " and is shipped to the House of Desperation in "
                << "Shelbyville.\n";
    else if (custArray[i].getHappiness() > MAX_HAP)
      wrap << '\t' << name << " enters a state of nirvana at level " 
                << level << " and decides to go to Shelbyville to lord over "
                << "the depairati in the House of Desperation.\n";

    // Write back into array if not going to Shelbyville
    else
    {
      custArray[survivors] = custArray[i];

      // Set new custNum
      custNum = ++survivors;
    }
  }

  return;
}


std::string aOrAn(const std::string& noun)
{
  bool vowel = false;
  char first = tolower(noun[0]);
  std::string toReturn;

  if (first == 'a' || first == 'e' || first == 'i' || first == 'o' || 
      first == 'u')
    vowel = true;

  if (vowel)
    toReturn = "an " + noun;
  else
    toReturn = "a " + noun;

  return toReturn;
}
