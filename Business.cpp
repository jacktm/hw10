// Jack Moore 11 Nov 2013
// Customer.h
// Header for Business class



#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include "Business.h"
#include "Customer.h"
#include "Product.h"



Business::Business()
{
  m_name = "";
  m_money = 0.0;
  m_invNum = 0;
  m_shopNum = 0;
  return;
}


Business::Business(const char uNmae[], const float uMoney, const char inv[])
{
  std::ifstream fin;
  int counter = 0;
  char tempString[100];

  m_name = uNmae;
  m_money = uMoney;

  // Open the inventory file
  fin.open(inv);

  // Check if valid file
  if (!fin)
  {
    std::cout << "Fatal Error: Could not open file " << inv << std::endl;
    exit(1);
  }

  // Read the inventory file
  while (!fin.eof() && counter < ITEMS)
  {
    // Get money value of product
    fin >> m_inventory[counter].m_price;

    // Remove extra space
    fin.ignore(256, ' ');

    // Get name of product
    fin.getline(tempString, 100);
    m_inventory[counter].m_name = tempString;
    
    counter++;
  }

  m_invNum = counter;
  m_shopNum = 0;

  // Close the file
  fin.close();

  return;
}


void Business::print() const
{
  std::cout << m_name << "\n\n";
  std::cout << "Money in cash register $" << m_money << ".\n\n";

  // Print items in inventory
  std::cout << "Inventory:" << std::endl;
  for (int i = 0; i < m_invNum; i++)
    std::cout << '\t' << m_inventory[i].m_name << " $" << m_inventory[i].m_price
              << std::endl;

  return;
}


void Business::addCustomer(const Customer& newCust)
{
  // Check if store is full
  if (m_shopNum < CUSTOMERS)
  {
    m_shoppers[m_shopNum] = newCust;
    m_shopNum++;
  }

  return;
}


void Business::sell_stuff()
{
  int item;

  // Parse through each customer
  for (int i = 0; i < m_shopNum; i++)
  {
    // Pick random item
    item = rand() % m_invNum;

    // Check if customer has enough m_money and if they have enough room in 
    // their shopping cart. Note the use of lazy execution to prevent accidental
    // purchases
    if (m_shoppers[i].getMoney() >= m_inventory[item].m_price 
        && m_shoppers[i].buy_something(m_inventory[item]))
    {
      // Update customer m_money
      m_shoppers[i].setMoney(m_shoppers[i].getMoney() - 
                             m_inventory[item].m_price);

      // Update cash register
      m_money += m_inventory[item].m_price;
    }
  }

  return;
}


void Business::customers_leave(Customer custArray[], int& custNum)
{
  // Do copy operation
  for (int i = 0; i < m_shopNum; i++)
    custArray[i] = m_shoppers[i];

  custNum = m_shopNum;

  // "Delete all customers"
  m_shopNum = 0;

  return;
}
