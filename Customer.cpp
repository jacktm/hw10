// Jack Moore 11 Nov 2013
// Customer.cpp
// Function definitions for Customer class



#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include "Customer.h"
#include "hw10Header.h"
#include "Product.h"
#include "Wordwrap.h"



Customer::Customer()
{
  m_purchNum = 0;
  m_money = rand() % static_cast<int>(MAX_MON - MIN_MON) + MIN_MON;
  strcpy(m_name, DEFAULT_NAME);
  m_inclination = 0;
  m_happiness = rand() % static_cast<int>(NIRVANA - DESPAIR) + DESPAIR;
  return;
}


bool Customer::buy_something(const product& item)
{
  bool toReturn = false;
  Wordwrap wrap;

  // 50/50 chance of purchase
  if (rand() % 2)
  {
    // Check for full pocket
    if (m_purchNum < MAX_PURCH)
    {
      ///////////////////////
      // OUTPUT 2
      ///////////////////////
      wrap << '\t' << m_name << " purchases " << aOrAn(item.m_name) 
                << " for $" << item.m_price << ".\n";

      toReturn = true;
      m_purchases[m_purchNum] = item;
      m_purchNum++;

      // Update happiness
      m_happiness += BUY_HAPP_INC;
    }
    else
    {
      m_happiness -= BUY_HAPP_DEC;
    }
  }
  return toReturn;
}


std::ostream & operator << (std::ostream & out, const Customer & source)
{
  Wordwrap wrap = Wordwrap(out, 72, 8, true);

  wrap << source.m_name << " has $" << source.m_money;

  // Check purchNum for grammatical listing

  // If 0: print .
  if (source.m_purchNum == 0)
    wrap << '.';

  // If only 1 print: obj1.
  else if (source.m_purchNum == 1)
    wrap << " and " << aOrAn(source.m_purchases[0].m_name) << '.';

  // If only 2 print: obj1 and obj2.
  //else if (source.m_purchNum == 2)
  //  out << " and purchases " << source.m_purchases[0].m_name << " and " 
  //            << source.m_purchases[1].m_name << '.';

  // If more than 2 print: obj1, obj2,... and obj3. 
  else
  {
    wrap << ", ";
    for (int i = 0; i < source.m_purchNum-1; i++)
      wrap << aOrAn(source.m_purchases[i].m_name) << ", ";
    wrap << "and " << aOrAn(source.m_purchases[source.m_purchNum-1].m_name) 
        << '.';
  }
  return out;
}


void Customer::throwItem(Customer& target)
{
  Wordwrap wrap;

  // Check if an item can be thrown
  if (m_purchNum > 0)
  {
    ///////////////////////
    // OUTPUT 3
    ///////////////////////
    wrap << '\t' << m_name << " throws " 
              << aOrAn(m_purchases[m_purchNum-1].m_name) << " at " 
              << target.m_name << ".\n";

    // "Throw" the item
    m_purchNum--;

    // Change happiness
    m_happiness += THROW_HAPP_INC;
    target.m_happiness -= TARGET_HAPP_DEC;
  }
  // If throw failed
  else
  {
    ///////////////////////
    // OUTPUT 3
    ///////////////////////
    wrap << '\t' << m_name << " tries to throw something at " << target.m_name 
              << " but has nothing to throw.\n";

    m_happiness -= THROW_FAIL_HAPP_DEC;
  }

  return;
}


void Customer::stealItem(Customer& victim)
{
  Wordwrap wrap;

  // Check if victim has something to steal and that there is room for item
  if (victim.m_purchNum > 0 && m_purchNum < MAX_PURCH)
  {
    ///////////////////////
    // OUTPUT 3
    ///////////////////////
    wrap << '\t' << m_name << " steals " 
              << aOrAn(victim.m_purchases[victim.m_purchNum-1].m_name)
              << " from " << victim.m_name << ".\n";

    // Execute "steal"
    m_purchases[m_purchNum] = victim.m_purchases[victim.m_purchNum-1];
    m_purchNum++;
    victim.m_purchNum--;

    // Change happiness
    m_happiness += STEAL_HAPP_INC;
    victim.m_happiness -= VICTIM_HAPP_DEC;
  }
  // If theft failed
  else
    ///////////////////////
    // OUTPUT 3
    ///////////////////////
    wrap << '\t' << m_name << " tries to steal something from " 
              << victim.m_name << " but fails.\n";

    m_happiness -= STEAL_FAIL_HAPP_DEC;

  return;
}
